import { CellMatrix } from "../../../sharedTypes";

/**
 * Fresh board for new games
 */
export const newBoard = [
  [null, null, null],
  [null, null, null],
  [null, null, null],
];

/**
 * Search the board for a winning symbol and return it
 */
const searchMatrix = (arrays: CellMatrix): string | null => {
  for (let x = 0; x < arrays.length; x++) {
    if (arrays[x].every((v) => v && v === arrays[x][0])) {
      return arrays[x][0];
    }
  }
  return null;
};

/**
 * Return array of winning cell arrays
 */
const getWinningArrays = (matrix: CellMatrix): CellMatrix => {
  return [
    /*Horizontal*/
    matrix[0],
    matrix[1],
    matrix[2],

    /*Vertical*/
    [matrix[0][0], matrix[1][0], matrix[2][0]],
    [matrix[0][1], matrix[1][1], matrix[2][1]],
    [matrix[0][2], matrix[1][2], matrix[2][2]],

    /*Diagonal*/
    [matrix[0][0], matrix[1][1], matrix[2][2]],
    [matrix[0][2], matrix[1][1], matrix[2][0]],
  ];
};

/**
 * Return "X" or "O" if there's a winner
 */
export const getWinningSymbol = (matrix: CellMatrix): string | null => {
  return matrix.length ? searchMatrix(getWinningArrays(matrix)) : null;
};
