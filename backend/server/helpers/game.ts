import { MoveData } from "../../../sharedTypes";
import { Socket } from "socket.io";

const symbols = ["X", "O"];
let players: string[] = []; // socket id's of players
let playerIndex = 0;

export const addPlayer = (socket: Socket): void => {
  players.push(socket.id);
};
export const removePlayer = (socket: Socket): string[] =>
  players.filter((id) => socket.id === id);

const getCurrentSymbol = (): string => symbols[playerIndex];

export const getTotalPlayers = (): number => players.length;
export const setPlayers = (newPlayers: string[]): void => {
  players = newPlayers;
};

export const getCurrentPlayer = (): string => players[playerIndex];
export const nextPlayer = (): void => {
  playerIndex = (playerIndex + 1) % 2;
};

export const getMoveData = (): MoveData => ({
  player: getCurrentPlayer(),
  symbol: getCurrentSymbol(),
});
