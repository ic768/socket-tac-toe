import express from "express";
import { createServer } from "http";
import { Socket, Server } from "socket.io";

import { CellMatrix } from "../../sharedTypes";

import { getWinningSymbol, newBoard } from "./helpers/board";
import {
  removePlayer,
  addPlayer,
  getCurrentPlayer,
  nextPlayer,
  getTotalPlayers,
  setPlayers,
  getMoveData,
} from "./helpers/game";

const port = process.env.PORT || 3001;
const app = express();
const server = createServer(app);
const io = new Server(server);

io.on("connection", (socket: Socket) => {
  addPlayer(socket);
  console.log(`Player ${getTotalPlayers()} joined`);

  socket.on("disconnect", () => {
    setPlayers(removePlayer(socket));
    console.log(`Player left - ${getTotalPlayers()} remaining`);
  });

  if (getTotalPlayers() === 2) {
    // start new game
    io.sockets.emit("newGame", {
      board: newBoard,
      ...getMoveData(),
    });
    console.log("starting new game");
  }

  socket.on("newMove", (board: CellMatrix) => {
    if (socket.id === getCurrentPlayer()) {
      // if move has been made (on correct player's turn)
      nextPlayer();
      io.sockets.emit("updateGame", {
        board,
        winner: getWinningSymbol(board),
        ...getMoveData(),
      });
    }
  });
});

server.listen(port, () => console.log(`Listening on port ${port}`));
