import { CellArray } from "../../../../sharedTypes";
import { IBoard } from "../../types";
import Row from "../Row";
import "./Board.css";

/**
 * Game board consisting of three rows
 */
const Board: React.FC<IBoard> = ({
  isYourTurn,
  cellMatrix,
  sendMatrix,
  currentSymbol,
}) => (
  <div className="App">
    <div className="board-container">
      <Row
        isYourTurn={isYourTurn}
        currentSymbol={currentSymbol}
        cells={cellMatrix[0]}
        sendMatrix={(updatedArray: CellArray) =>
          sendMatrix([updatedArray, cellMatrix[1], cellMatrix[2]])
        }
      />
      <Row
        isYourTurn={isYourTurn}
        currentSymbol={currentSymbol}
        cells={cellMatrix[1]}
        sendMatrix={(updatedArray: CellArray) =>
          sendMatrix([cellMatrix[0], updatedArray, cellMatrix[2]])
        }
      />
      <Row
        isYourTurn={isYourTurn}
        currentSymbol={currentSymbol}
        cells={cellMatrix[2]}
        sendMatrix={(updatedArray: CellArray) =>
          sendMatrix([cellMatrix[0], cellMatrix[1], updatedArray])
        }
      />
    </div>
  </div>
);
export default Board;
