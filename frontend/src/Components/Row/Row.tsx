import classNames from "classnames";
import { IRow } from "../../types";
import "./Row.css";

/**
 * Single row of game board
 */
const Row: React.FC<IRow> = ({
  isYourTurn,
  currentSymbol,
  cells,
  sendMatrix,
}) => {
  const cellClasses = classNames({
    cell: true,
    "is-playing": isYourTurn,
  });

  return (
    <div className="row">
      {cells &&
        cells.map((c, i) => (
          <div
            key={i}
            className={cellClasses}
            onClick={() => {
              !c &&
                sendMatrix(
                  cells.map((c, ind) => (i === ind && !c ? currentSymbol : c))
                );
            }}
          >
            <span>{c}</span>
          </div>
        ))}
    </div>
  );
};

export default Row;
