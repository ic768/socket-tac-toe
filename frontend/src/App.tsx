import { useState, useEffect } from "react";
import io, { Socket } from "socket.io-client";

import Board from "./Components/Board";
import { CellMatrix, GameData } from "./../../sharedTypes";

import "./App.css";

function App() {
  const [socket, setSocket] = useState<Socket | undefined>(); // Web socket used for data transmission
  const [winner, setWinner] = useState<number | null>(null);
  const [cellMatrix, setCellMatrix] = useState<CellMatrix>([]); // Game board data
  const [currentSymbol, setCurrentSymbol] = useState<string>("X");
  const [isYourTurn, setIsYourTurn] = useState<boolean>(false);

  // Play if it's your turn
  const sendMatrix = (newMatrix: CellMatrix): void => {
    console.log("new move");
    socket && isYourTurn && socket.emit("newMove", newMatrix);
  };
  socket && console.log(socket.id);

  useEffect(() => {
    if (!socket) {
      setSocket(io("/"));
    } else {
      const updateGame = (data: GameData) => {
        const { player, board, symbol, winner } = data;
        setIsYourTurn(socket.id === player);
        setCellMatrix(board);
        setCurrentSymbol(symbol);
        winner && setWinner(winner);
      };

      socket.on("newGame", updateGame);
      socket.on("updateGame", updateGame);
    }
  }, [socket]);

  return (
    <div className="App">
      <div className="turn-banner">
        It is your {!isYourTurn && "opponent's "}turn
      </div>
      <Board
        isYourTurn={isYourTurn}
        currentSymbol={currentSymbol}
        cellMatrix={cellMatrix}
        sendMatrix={sendMatrix}
      />
      {winner && <div className="winner-banner">{winner} has won!</div>}
    </div>
  );
}

export default App;
