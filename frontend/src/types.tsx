import { Dispatch } from "react";
import { CellArray, CellMatrix } from "../../sharedTypes";

/**
 * Single row of the game board
 */
export interface IRow {
  isYourTurn: boolean;
  currentSymbol: string; //"X" or "O"
  cells: CellArray;
  sendMatrix: Dispatch<CellArray>;
}

/**
 * Entire game board
 */
export interface IBoard {
  isYourTurn: boolean;
  currentSymbol: string;
  cellMatrix: CellMatrix;
  sendMatrix: (NewMatrix: CellMatrix) => void;
}
