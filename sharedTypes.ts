/*Types shared between backend and frontend */

/**
 * Array of tic-tac-toe cells - data equivalent to a board row
 */
export type CellArray = Array<null | string>;
/**
 * Matrix of tic-tac-toe cells - data equivalent to the whole game board
 */
export type CellMatrix = Array<CellArray>;

/**
 * Who's playing what symbol on the current move
 */
export type MoveData = {
  player: string;
  symbol: string;
};

/**
 * Data sent to and from server to advance the game
 */
export type GameData = MoveData & {
  board: CellMatrix;
  winner?: number | null;
};
